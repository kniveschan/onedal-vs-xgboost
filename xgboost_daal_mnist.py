import timeit
import numpy as np
import pandas as pd

NREPEAT = 10

def box_filter(timing, left=0.25, right=0.75):
    timing.sort()
    size = len(timing)
    if size == 1:
        return timing[0]

    Q1, Q2 = timing[int(size * left)], timing[int(size * right)]

    IQ = Q2 - Q1

    lower = Q1 - 1.5 * IQ
    upper = Q2 + 1.5 * IQ

    result = np.array([item for item in timing if lower < item < upper])
    return np.mean(result)


df = pd.read_csv("mnist_train.csv", header=None)
print("train size {}".format(df.shape))

X_train = np.ascontiguousarray(df[df.columns[:-1]].values, dtype=np.float32)
y_train = np.ascontiguousarray(df[df.columns[-1]].values, dtype=np.float32).reshape((df.shape[0], 1))

df = pd.read_csv("mnist_test.csv", header=None)
print("test size {}".format(df.shape))

X_test = np.ascontiguousarray(df[df.columns[:-1]].values, dtype=np.float32)
y_test = np.ascontiguousarray(df[df.columns[-1]].values, dtype=np.float32)

dxgb_daal_params = {
    'nClasses':                     10,
    'fptype':                       'float',
    'maxIterations':                50,
    'maxTreeDepth':                 6,
    'minSplitLoss':                 0.1,
    'shrinkage':                    0.4,
    'observationsPerTreeFraction':  1,
    'lambda_':                      2.0,
    'maxBins':                      256,
    'featuresPerNode':              0,
    'minBinSize':                   5,
    'memorySavingMode':             False,
    'minObservationsInLeafNode':    1
}

dxgb_cpu_params = {
    'silent':                       1,
    'num_class':                    10,
    'n_estimators':                 50,
    'max_depth':                    6,
    'min_split_loss':               0.1,
    'learning_rate':                0.4,
    'subsample':                    1,
    'reg_lambda':                   2.0,
    'tree_method':                  'hist',
    'predictor':                    'cpu_predictor',
    'max_bin':                      256,
    "min_child_weight":             1,
    "verbosity":                     0
}

print("*******	Performing DAAL GBT CPU ********")
import daal4py as d4p

def gbt_daal4py_fit():
    global train_result
    train_algo = d4p.gbt_classification_training(**dxgb_daal_params)
    train_result = train_algo.compute(X_train, y_train)

def gbt_daal4py_predict_of_train_data():
    global result_predict_daal_train
    predict_algo = d4p.gbt_classification_prediction(nClasses=10, fptype='float')
    result_predict_daal_train = predict_algo.compute(X_train, train_result.model).prediction.ravel()

def gbt_daal4py_predict_of_test_data():
    global result_predict_daal_test
    predict_algo = d4p.gbt_classification_prediction(nClasses=10, fptype='float')
    result_predict_daal_test = predict_algo.compute(X_test, train_result.model).prediction.ravel()

t = timeit.Timer(stmt="%s()" % gbt_daal4py_fit.__name__, setup="from __main__ import %s" % gbt_daal4py_fit.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("1.	DAAL GBT training ", timing)

t = timeit.Timer(stmt="%s()" % gbt_daal4py_predict_of_train_data.__name__,
    setup="from __main__ import %s" % gbt_daal4py_predict_of_train_data.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("2.	DAAL GBT predict train data ", timing)

t = timeit.Timer(stmt="%s()" % gbt_daal4py_predict_of_test_data.__name__, setup="from __main__ import %s" % gbt_daal4py_predict_of_test_data.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("3.	DAAL GBT predict test data ", timing)

print("---------------")

print("*******	Performing XGBoost CPU ********")
import xgboost as xgb

def xbg_fit():
    global model_xgb
    dtrain = xgb.DMatrix(X_train, label=y_train)
    model_xgb = xgb.train(dxgb_cpu_params, dtrain, dxgb_cpu_params['n_estimators'])

def xgb_predict_of_train_data():
    global result_predict_xgboot_train
    dtest = xgb.DMatrix(X_train)
    result_predict_xgboot_train = model_xgb.predict(dtest)

def xgb_predict_of_test_data():
    global result_predict_xgboot_test
    dtest = xgb.DMatrix(X_test)
    result_predict_xgboot_test = model_xgb.predict(dtest)

t = timeit.Timer(stmt="%s()" % xbg_fit.__name__, setup="from __main__ import %s" % xbg_fit.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("1.	XGBOOST training ", timing)

t = timeit.Timer(stmt="%s()" % xgb_predict_of_train_data.__name__, setup="from __main__ import %s" % xgb_predict_of_train_data.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("2.	XGBOOST predict train data ", timing)

t = timeit.Timer(stmt="%s()" % xgb_predict_of_test_data.__name__, setup="from __main__ import %s" % xgb_predict_of_test_data.__name__)
res = t.repeat(repeat=NREPEAT, number=1)
#print(res)
timing = 1000.0 * box_filter(res)
print("3.	XGBOOST predict test data ", timing)

print("---------------")

y_train = y_train.ravel()

print("TRAINING ERROR")
eq = y_train == result_predict_daal_train
print("	TP DAAL : {} ; Accuracy DAAL {:.4f} ".format(sum(eq), np.mean(eq)))
eq = y_train == result_predict_xgboot_train
print("	TP XGBT : {} ; Accuracy XGBT {:.4f} ".format(sum(eq), np.mean(eq)))
print("---------------")
print("TESTING ERROR")
eq = y_test == result_predict_daal_test
print("	TP DAAL : {} ; Accuracy DAAL {:.4f} ".format(sum(eq), np.mean(eq)))
eq = y_test == result_predict_xgboot_test
print("	TP XGBT : {} ; Accuracy XGBT {:.4f} ".format(sum(eq), np.mean(eq)))
print("---------------")

